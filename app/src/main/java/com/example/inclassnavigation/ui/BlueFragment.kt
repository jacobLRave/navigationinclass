package com.example.inclassnavigation.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.inclassnavigation.R
import com.example.inclassnavigation.databinding.FragmentBlueBinding

class BlueFragment : Fragment() {

    private var _binding: FragmentBlueBinding? = null
    private val binding get() = _binding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentBlueBinding.inflate(inflater,container,false).also {
        _binding = it
    }.root

    override  fun onDestroyView(){
        super.onDestroyView()
        _binding = null
    }
}