package com.example.inclassnavigation.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.inclassnavigation.R
import com.example.inclassnavigation.databinding.FragmentMainBinding

class MainFragment : Fragment() {

    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentMainBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.cvBlue.setOnClickListener {
            findNavController().navigate(R.id.action_mainFragment_to_blueFragment)
        }
        binding.cvGreen.setOnClickListener {
            findNavController().navigate(R.id.action_mainFragment_to_greenFragment)
        }

        binding.cvRed.setOnClickListener {
            findNavController().navigate(R.id.action_mainFragment_to_redFragment)
        }

        binding.cvGray.setOnClickListener {
            findNavController().navigate(R.id.action_mainFragment_to_grayFragment22)
        }

        binding.cvPurple.setOnClickListener {
            findNavController().navigate(R.id.action_mainFragment_to_purpleFragment)
        }

        binding.cvYellow.setOnClickListener {
            findNavController().navigate(R.id.action_mainFragment_to_yellowFragment)
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}